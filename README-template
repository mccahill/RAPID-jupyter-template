This VM has been configured to run 5 Jupyter instances via set of Docker 
containers. This makes it easy to move the application to other servers, 
reproduce the installation steps, or even modify the installation in a 
reproducible way.

To start using the stock Jupyter installation, see below 
for connection information. The file
     README-how-to-build-Jupyter 
has details on how to reinstall/update the default installation.

If you cannot connect to the server, it is probably because the Docker
containers are not running. You can start them up like this:

     cd /srv/persistent-data/docker-scripts
	 ./run-everything

To verify that all the containers are running ask docker for a list of
processes:

     sudo docker ps
	 
You should see serveral r-studio images running, along with a image named
docker-gen and an nginx image.

If the docker daemon is not running, start it like this:

     sudo service docker start
	 

How to Connect
--------------

To automate setup of this server, it has been installed with a self-signed
site certificate. This will make some web browsers warn you that the site may
be insecure, and you may have to visit your web browser's advanced settings
to tell it to trust the server.

Below is the connect information for the Jupyter web servers running 
on this server. By default 5 Jupyter instances are run.

Note: this image includes mysql and the mysql database has two users who's
passwords should be changed from the default. See the file
     README-how_to_build-Jupyter
for information on how to chnage the default mysql passwords.



#!/bin/bash

#
# get to the current version of the OS
#
export  
sudo apt-get update 
sudo DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade -y 
sudo apt-get autoremove -y

#
# add the VMware Open VM Tools
#
# mkdir /tmp/vmware-keys
# cd /tmp/vmware-keys
# wget https://packages.vmware.com/tools/keys/VMWARE-PACKAGING-GPG-DSA-KEY.pub
# wget https://packages.vmware.com/tools/keys/VMWARE-PACKAGING-GPG-RSA-KEY.pub
# sudo apt-key add /tmp/vmware-keys/VMWARE-PACKAGING-GPG-DSA-KEY.pub
# sudo apt-key add /tmp/vmware-keys/VMWARE-PACKAGING-GPG-RSA-KEY.pub
# echo 'deb http://packages.vmware.com/packages/ubuntu '$(lsb_release -sc) 'main' > /tmp/vmware-keys/vmware-tools.list
# sudo cp /tmp/vmware-keys/vmware-tools.list /etc/apt/sources.list.d/vmware-tools.list
# sudo apt-get update
# sudo apt-get install open-vm-tools-deploypkg -y
# rm -rf /tmp/vmware-keys

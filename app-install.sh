#!/bin/bash

# Install Docker, NGINX, Docker-gen, Jupyter, MySQL

# We assume that you do a git clone of the repository, cd to the
# directory and run this script from there
#
# where are we being run from?
#
INSTALLFROM=~rapiduser/src

#
# copy the app-specific personalize_me script into place
# this will be run by RAPID when a new instance of the VM is
# cloned from the vmware template 
#
sudo cp $INSTALLFROM/personalize_me.sh /usr/local/bin/personalize_me
sudo chmod ugo+x /usr/local/bin/personalize_me


#
# install the docker daemon
#
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo sh -c 'echo "deb https://apt.dockerproject.org/repo ubuntu-"$(lsb_release -sc) " main" > /etc/apt/sources.list.d/docker.list'
sudo apt-get update
sudo apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get update
sudo apt-get install -y csh
sudo apt-get install -y docker-engine


#
# install directories to hold docker scripts and persistent home directories
#
sudo chmod go+w /srv
mkdir /srv/persistent-data
mkdir /srv/persistent-data/src
mkdir /srv/persistent-data/homedirs
mkdir /srv/persistent-data/docker-scripts

#
#  create home directories for the docker container users 
#
for INDEX in 0 1 2 3 4
do
  USERID=`printf "%03d" $INDEX`
  cp -r $INSTALLFROM/homedirs/user-template /srv/persistent-data/homedirs/user$USERID
done



#
# get the docker container source and compile it
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/jupyter-mysql-notebook.git
cd /srv/persistent-data/src/jupyter-mysql-notebook
git submodule init
git submodule update
git submodule foreach git pull origin master
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/mysql-notebook
cp Dockerfile.everything Dockerfile
./build
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/docker-nginx
git pull origin duke
./build
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/docker-gen
./build


#
# copy docker start/stop scripts into place
#
cp -r $INSTALLFROM/docker-scripts/* /srv/persistent-data/docker-scripts/


#
# write out the README files in the homedirectory for the rapiduser
#
cp $INSTALLFROM/README-how_to_build-Jupyter ~rapiduser/
cp $INSTALLFROM/README-template ~rapiduser/README


#
# personalize this instance
#
sudo /usr/local/bin/personalize_me


#
# start the docker containers for the application
#
cd /srv/persistent-data/docker-scripts
./run-everything



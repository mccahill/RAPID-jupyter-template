#!/bin/bash

# Install Docker, NGINX, Docker-gen, Jupyter, MySQL

# We assume that you do a git clone of the repository, cd to the
# directory and run this script from there
#
# where are we being run from?
#
INSTALLFROM=/srv/persistent-data/src

#
# copy the app-specific personalize_me script into place
# this will be run by RAPID when a new instance of the VM is
# cloned from the vmware template 
#
sudo cp $INSTALLFROM/personalize_me.sh /usr/local/bin/personalize_me
sudo chmod ugo+x /usr/local/bin/personalize_me

#
#  create home directories for the docker container users 
#
for INDEX in 0 1 2 3 4
do
  USERID=`printf "%03d" $INDEX`
  cp -r $INSTALLFROM/homedirs/user-template /srv/persistent-data/homedirs/user$USERID
done


#
# get the scratch mysql database
#
cd /srv/persistent-data
wget https://people.duke.edu/~mccahill/RAPID-Jupyter-mysql_persistant_data.tgz
sudo tar -xzvf RAPID-Jupyter-mysql_persistant_data.tgz
rm RAPID-Jupyter-mysql_persistant_data.tgz


#
# get the docker container sources but don't compile them, we will grab pre-build docker images
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/jupyter-mysql-notebook.git
cd /srv/persistent-data/src/jupyter-mysql-notebook
git submodule init
git submodule update
git submodule foreach git pull origin master
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/mysql-notebook
cp Dockerfile.everything Dockerfile
# ./build
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/docker-nginx
git pull origin duke
# ./build
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/docker-gen
# ./build
cd /srv/persistent-data/src/jupyter-mysql-notebook/src/mysql/5.5
# sudo docker build -t="mysql" .


#
# fetch the docker container images
#
mkdir /srv/persistent-data/docker-images
cd /srv/persistent-data/docker-images
wget https://people.duke.edu/~mccahill/RAPID-mysql.tar
wget https://people.duke.edu/~mccahill/RAPID-docker-gen.tar
wget https://people.duke.edu/~mccahill/RAPID-nginx.tar
wget https://people.duke.edu/~mccahill/RAPID-jupyter-sql.tar

#
# load the images
#
sudo docker load -i ./RAPID-mysql.tar
sudo docker load -i ./RAPID-docker-gen.tar
sudo docker load -i ./RAPID-nginx.tar
sudo docker load -i ./RAPID-jupyter-sql.tar

#
# remove the tar files to save some space
#
rm -rf /srv/persistent-data/docker-images

#
# copy docker start/stop scripts into place
#
cp -r $INSTALLFROM/docker-scripts/* /srv/persistent-data/docker-scripts/


#
# write out the README files in the homedirectory for the rapiduser
#
cp $INSTALLFROM/README-how_to_build-Jupyter ~rapiduser/
cp $INSTALLFROM/README-template ~rapiduser/README


#
# personalize this instance
#
sudo /usr/local/bin/personalize_me


#
# leave a flag saying we got done
#
sudo cp /dev/null /srv/persistent-data/docker-scripts/app-install-done

#
# start the docker containers for the application
#
# cd /srv/persistent-data/docker-scripts
# ./run-everything


